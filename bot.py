import logging
from telegram import Update
from telegram.ext import ApplicationBuilder, ContextTypes, CommandHandler, MessageHandler, filters
from session import ViewingEvents, add_session, get_session, remove_session, StateIds, VotingOptions, Start
from functools import reduce
import api
import os

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)

BOT_TOKEN = os.environ.get("BOT_TOKEN", "ChangeMe")

async def start(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await context.bot.send_message(chat_id=update.effective_chat.id, text='''LAST SUPPER BOT - comandos
    /login <email> <contraseña> para iniciar sesión
    /current_session para ver token de la sesión actual
    /logout para cerrar sesión
    Eventos: escriba A para crear evento, B para ver eventos e Inicio para regresar al inicio''')

async def login(update: Update, context: ContextTypes.DEFAULT_TYPE):
    res = api.login(context.args[0], context.args[1])
    add_session(update.effective_chat.id, res['access_token'], res['user']['id'])
    await context.bot.send_message(chat_id=update.effective_chat.id, text='Sesión iniciada')

async def current_session(update: Update, context: ContextTypes.DEFAULT_TYPE):
    token = get_session(update.effective_chat.id).token
    await context.bot.send_message(chat_id=update.effective_chat.id, text=token)

async def logout(update: Update, context: ContextTypes.DEFAULT_TYPE):
    token = get_session(update.effective_chat.id).token
    api.logout(token)
    remove_session(update.effective_chat.id)
    await context.bot.send_message(chat_id=update.effective_chat.id, text='Sesión finalizada')

async def msg(update: Update, context: ContextTypes.DEFAULT_TYPE):
    text = update.message.text
    session = get_session(update.effective_chat.id)
    new_state_id = session.state.interpret_text(text)

    # INICIO
    if new_state_id == StateIds.START:
        session.state = Start()
        await context.bot.send_message(chat_id=update.effective_chat.id, text='Escriba A para crear evento, B para ver eventos, Inicio para volver al inicio')

    # VISTA DE EVENTOS
    if new_state_id == StateIds.VIEWING_EVENTS:
        events = api.get_events(session.token)
        session.state = ViewingEvents(events)
        indexed_events = []
        for i, event in enumerate(events):
            indexed_events.append((i+1, event))
        events_as_str = reduce(lambda string, event: string + str(event[0]) + ": " + str(event[1]['name']) + "\n", indexed_events, "")
        await context.bot.send_message(chat_id=update.effective_chat.id, text='Eventos: ' + events_as_str)

    # VISTA DE OPCIONES
    if new_state_id == StateIds.VOTING_OPTIONS:
        event = None
        if session.state.state_id() == StateIds.VOTING_OPTIONS:
            if not text.isdigit(): return
            event = session.state.event
            option_id = event['options_available'][int(text) - 1]['id']
            api.vote_option(session.token, session.user_id, event['id'], option_id)
            event = api.get_event(session.token, event['id'])
        if session.state.state_id() == StateIds.VIEWING_EVENTS:
            event = api.get_event(session.token, session.state.events[int(text) - 1]['id'])

        session.state = VotingOptions(event)
        
        indexed_options = []
        for i, option in enumerate(event['options_available']):
            indexed_options.append((i+1, option))
        options_as_str = reduce(lambda string, option: string + str(option[0]) + ": " + str(option[1]['datetime']) + "\n", indexed_options, "")
        options_as_str = options_as_str + "\nOpciones ya votadas (no pueden volver a votarse):\n"
        options_as_str = reduce(lambda string, option: string + option['datetime'] + "\n", event['options_voted'], options_as_str)
        await context.bot.send_message(chat_id=update.effective_chat.id, text = 'Opciones: ' + options_as_str)

if __name__ == '__main__':
    application = ApplicationBuilder().token(BOT_TOKEN).build()
    
    start_handler = CommandHandler('start', start)
    application.add_handler(start_handler)

    msg_handler = MessageHandler(filters.TEXT & (~filters.COMMAND), msg)
    application.add_handler(msg_handler)

    login_handler = CommandHandler('login', login)
    application.add_handler(login_handler)

    current_session_handler = CommandHandler('current_session', current_session)
    application.add_handler(current_session_handler)

    logout_handler = CommandHandler('logout', logout)
    application.add_handler(logout_handler)
    
    application.run_polling()