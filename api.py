import requests
import os

BACKEND_URL = os.environ.get("BACKEND_URL", "https://api.lastsupper.online")

def login(email: str, password: str):
    body={'username': email, 'password': password}
    headers={'Content-Type': 'application/json'}
    response = requests.post(f'{BACKEND_URL}/users/signin', json=body, headers=headers)
    return response.json()

def logout(token: str):
    headers={'Authorization': 'Bearer ' + token, 'Content-Type': 'application/json'}
    requests.post(f'{BACKEND_URL}/users/logout', headers=headers)
    return

def get_events(token: str):
    headers={'Authorization': 'Bearer ' + token, 'Content-Type': 'application/json'}
    response = requests.get(f'{BACKEND_URL}/events', headers=headers)
    return response.json()['events']

def vote_option(token: str, user_id: str, event_id: str, option_id: str):
    headers={'Authorization': 'Bearer ' + token, 'Content-Type': 'application/json'}
    body={'voter_id': user_id, 'option_ids': [option_id]}
    requests.post(f'{BACKEND_URL}/events/' + event_id + '/options/votes', headers=headers, json=body)
    return

def get_event(token: str, event_id: str):
    headers={'Authorization': 'Bearer ' + token, 'Content-Type': 'application/json'}
    response = requests.get(f'{BACKEND_URL}/events/' + event_id + '/available', headers=headers)
    return response.json()

# GET /events/{id-evento}/available


# {
#     "available": true,
#     "created_at": "Thu, 06 Jul 2023 22:34:01 GMT",
#     "creator": {
#         "id": "64a74159e77b9b0d1a390278",
#         "username": "johndoe"
#     },
#     "final_date": null,
#     "id": "64a74159e77b9b0d1a39027d",
#     "name": "New Event",
#     "options_available": [
#         {
#             "datetime": "2023-06-19 10:13:00",
#             "id": "64a74159e77b9b0d1a39027f",
#             "votes": 0
#         }
#     ],
#     "options_voted": [
#         {
#             "datetime": "2023-06-19 10:12:00",
#             "id": "64a74159e77b9b0d1a39027e",
#             "votes": 1
#         }
#     ]
# }