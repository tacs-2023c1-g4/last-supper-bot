### Interacting with the bot
Talk with the bot at `t.me/last_supper_bot` when it's deployed.


# How to run?


## Standalone
```
docker build -t tacs-bot .
docker run tacs-bot
```

## Last-Supper project

Copy `example.env` to `.env` and replace the token of the bot.

Extract the compose to the root folder contianing all the repos of the project. And run:

```
docker compose build
docker compose up -d
```

## Commands
`/start`: displays command info
`/login <user_email> <user_password>`: logs user in
`/current_session`: displays session token
`/logout`: logs current user out

## Text-based interaction
From the starting state, type "B" to display all the events in the system, accompanied by numbers. Type an event's number to display the event's options, accompanied by numbers. Type an option's number to vote it. At any step, type "Inicio" to return to the starting events display.
