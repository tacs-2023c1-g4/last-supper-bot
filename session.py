from abc import ABC, abstractmethod
from enum import Enum

sessions = dict()

class StateIds(Enum):
    START = 1
    VIEWING_EVENTS = 2
    VOTING_OPTIONS = 3
    CREATING_EVENT = 4
    CREATING_OPTIONS = 5

class State(ABC):
    @abstractmethod
    def state_id(self): pass
    @abstractmethod
    def interpret_text(self, text: str): pass

class Start(State):
    def state_id(self): return StateIds.START
    def interpret_text(self, text: str):
        if text == 'B':
            return StateIds.VIEWING_EVENTS
        if text == 'A':
            return StateIds.CREATING_EVENT
        return StateIds.START

class VotingOptions(State):
    def __init__(self, event):
        self.event = event
    def state_id(self): return StateIds.VOTING_OPTIONS
    def interpret_text(self, text: str):
        if text == 'Inicio':
            return StateIds.START
        return StateIds.VOTING_OPTIONS
        
class ViewingEvents(State):
    events: list
    def __init__(self, events):
        self.events = events
    def state_id(self): return StateIds.VIEWING_EVENTS
    def interpret_text(self, text: str):
        if text == 'Inicio':
            return StateIds.START
        if text.isdigit():
            return StateIds.VOTING_OPTIONS
        return StateIds.VIEWING_EVENTS

class Session():
    token: str
    state: State
    user_id: str
    def __init__(self, token, user_id):
        self.token = token
        self.state = Start()
        self.user_id = str(user_id)

def add_session(chat_id, token, user_id):
    global sessions
    sessions[chat_id] = Session(token, user_id)
    return

def remove_session(chat_id):
    global sessions
    if chat_id in sessions:
        del sessions[chat_id]
    return

def get_session(chat_id):
    global sessions
    if chat_id in sessions:
        return sessions[chat_id]
    else:
        return 'No cuenta con una sesión abierta'